package plugins.nchenouard.trackprocessorintensityprofile;

enum AveragingType {DISK, DISK_BACKGROUND_CORRECTED, GAUSSIAN_FIT2D, SPOT_MASK}