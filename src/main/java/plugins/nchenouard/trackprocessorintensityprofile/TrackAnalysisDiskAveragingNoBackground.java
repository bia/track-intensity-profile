package plugins.nchenouard.trackprocessorintensityprofile;

import java.util.ArrayList;
import java.util.Arrays;

import icy.sequence.Sequence;

import org.jfree.data.xy.XYSeries;

import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;

public class TrackAnalysisDiskAveragingNoBackground extends TrackAnalysis
{
	XYSeries[] meanIntensity;
	XYSeries[] minIntensity;
	XYSeries[] maxIntensity;
	XYSeries[] medianIntensity;
	XYSeries[] sumIntensity;
	XYSeries[] varIntensity;

	TrackAnalysisDiskAveragingNoBackground(TrackSegment track, Sequence sequence,
			String description, AveragingType averagingMethod) {
		super(track, sequence, description, averagingMethod);

		meanIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			meanIntensity[c] = new XYSeries(description);
		minIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			minIntensity[c] = new XYSeries(description);
		maxIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			maxIntensity[c] = new XYSeries(description);
		medianIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			medianIntensity[c] = new XYSeries(description);
		sumIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			sumIntensity[c] = new XYSeries(description);
		varIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			varIntensity[c] = new XYSeries(description);
	}

	@Override
	public void clearSeries() {
		for (int c = 0; c < meanIntensity.length; c++)
			meanIntensity[c].clear();
		for (int c = 0; c < minIntensity.length; c++)
			minIntensity[c].clear();
		for (int c = 0; c < maxIntensity.length; c++)
			maxIntensity[c].clear();
		for (int c = 0; c < medianIntensity.length; c++)
			medianIntensity[c].clear();
		for (int c = 0; c < sumIntensity.length; c++)
			sumIntensity[c].clear();
		for (int c =  0; c < varIntensity.length; c++)
			varIntensity[c].clear();
	}

	//TODO: make it 3D
	public void fillAveragingSeriesAtT(int t, double[][][] imageTab, double diskRadius, double outterDiskRadius)
	{
		if (track.getFirstDetection().getT()<= t && track.getLastDetection().getT() >= t)
		{
			Detection d = track.getDetectionAtTime(t);
			if (d != null)
			{
				int width = sequence.getSizeX();
				int height = sequence.getSizeY();
				int depth = sequence.getSizeZ();

				ArrayList<Double> valueList = new ArrayList<Double>();
				for (int c = 0; c < sequence.getSizeC(); c++)
				{
					valueList.clear();
					int cntValues = 0;
					double sumIntensity = 0;
					double minIntensity = 0;
					double maxIntensity = 0;
					double sumSqIntensity = 0;
					double sumBackgroundIntensity = 0;
					int cntBackgroundValues = 0;

					int minX = (int) Math.max(0, d.getX() - outterDiskRadius);
					int minY = (int) Math.max(0, d.getY() - outterDiskRadius);
					int minZ = (int) Math.max(0, d.getZ() - diskRadius);

					int maxX = (int) Math.min(width - 1, Math.ceil(d.getX() + outterDiskRadius));
					int maxY = (int) Math.min(height - 1, Math.ceil(d.getY() + outterDiskRadius));
					int maxZ = (int) Math.min(depth - 1, Math.ceil(d.getZ() + diskRadius));

					for (int z = minZ; z <= maxZ; z++)
					{
						double[] imageArray = imageTab[z][c];
						for (int y = minY; y <= maxY; y++)
							for (int x = minX; x <= maxX; x++)					
							{
								if ((d.getX() - x)*(d.getX() - x) + (d.getY() - y)*(d.getY() - y) + (d.getZ() - z)*(d.getZ() - z)<= diskRadius*diskRadius)
								{
									cntValues++;
									double value  = imageArray[x + y*width];
									sumIntensity += value;
									if (cntValues == 1)
									{
										minIntensity = value;
										maxIntensity = value;
									}
									else
									{
										if (value < minIntensity)
											minIntensity = value;
										if (value > maxIntensity)
											maxIntensity = value;
									}
									sumSqIntensity += value*value;
									valueList.add(new Double(value));
								}
								else if ((d.getX() - x)*(d.getX() - x) + (d.getY() - y)*(d.getY() - y) + (d.getZ() - z)*(d.getZ() - z)<= outterDiskRadius*outterDiskRadius)
								{
									cntBackgroundValues ++;
									sumBackgroundIntensity += imageArray[x + y*width];
								}
							}
						double meanIntensity = 0;
						double varIntensity = 0;
						if (cntValues >0 )
						{
							meanIntensity = sumIntensity/(double)cntValues;
							varIntensity = sumSqIntensity/(double)cntValues - meanIntensity*meanIntensity;
							meanIntensity -= sumBackgroundIntensity/(double)cntBackgroundValues;
						}

						this.meanIntensity[c].add(t, meanIntensity);
						this.maxIntensity[c].add(t, maxIntensity);
						this.minIntensity[c].add(t, minIntensity);
						this.varIntensity[c].add(t, varIntensity);
						this.sumIntensity[c].add(t, sumIntensity);
						Arrays.sort(valueList.toArray());			
						if (cntValues == 1)
							this.medianIntensity[c].add(t, valueList.get(0));
						else if (cntValues > 0)
						{
							if (cntValues%2 == 0)
							{
								this.medianIntensity[c].add(t, (valueList.get((int) cntValues/2))+(valueList.get(((int) cntValues/2))-1)/2);
							}
							else
								this.medianIntensity[c].add(t, valueList.get((int) cntValues/2));
						}
						else
							this.medianIntensity[c].add(t, 0);
					}
				}
			}
		}
	}
}
