package plugins.nchenouard.trackprocessorintensityprofile;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.progress.AnnounceFrame;
import icy.sequence.Sequence;
import icy.type.collection.array.ArrayUtil;
import icy.util.XLSUtil;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;


/** A TrackProcessor to monitor detection fluorescence along time
 * 
 * @author nicolas chenouard
 * 
 * date 11/03/2012
 *
 * */

public class TrackProcessorIntensityProfile extends PluginTrackManagerProcessor
{
	//TODO: disable analysis thread when processor is disabled
	ArrayList<JFreeChart> charts;
	JPanel chartsPanel = new JPanel();
	JCheckBox displayLegendBox = new JCheckBox("Display legend");

	JPanel averagingDiskPanel;
	JPanel averagingGaussianFitPanel;
	JPanel averagingDiskNoBackgroundPanel;
	JPanel averagingMaskPanel;

	JComboBox<AveragingType> averagingTypeBox;
	CardLayout methodCardLayout;
	JPanel averagingMethodPanel;

	String[] criteria = new String[]{"Mean intensity", "Max intensity", "Min intensity", "Median intensity", "Intensity sum", "Intensity variance"};
	JComboBox<String> criteriaBox = new JComboBox<String>(criteria);
	JComboBox<String> criteriaBox2 = new JComboBox<String>(criteria);
	JComboBox<String> criteriaBox3 = new JComboBox<String>(criteria);

	SpinnerNumberModel diskDiameterModel = new SpinnerNumberModel(2, 0, 1000, 0.5);
	SpinnerNumberModel diskOutterDiameterModel = new SpinnerNumberModel(4, 2, 1000, 0.5);

	String[] criteriaGaussian = new String[]{"Amplitude", "Background"};
	JComboBox<String> criteriaGaussianBox = new JComboBox<String>(criteriaGaussian);
	JTextField standardDeviationTF;

	ArrayList<TrackAnalysis> analyzedTracks = new ArrayList<TrackAnalysis>();
	ArrayList<TrackAnalysis> toDisplayTracks = new ArrayList<TrackAnalysis>();

	ArrayList<int[]> positions = new ArrayList<int[]>();
	ReentrantLock positionsLock = new ReentrantLock();

	ReentrantLock analyzedTracksLock = new ReentrantLock();
	ArrayList<TrackSegment> seriesToFill = new ArrayList<TrackSegment>();

	ReentrantLock seriesToFillLock = new ReentrantLock();
	Condition seriesToFillCondition = seriesToFillLock.newCondition();

	FillSeriesThread fillSeriesThread;

	Sequence selectedSequence = null;

	public TrackProcessorIntensityProfile()
	{
		this.setName("Intensity profile");

		this.panel.setLayout(new BorderLayout());

		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());

		JPanel averagingTypePanel = new JPanel(new GridLayout(2, 1));
		averagingTypePanel.add(new JLabel("Averaging type"));
		averagingTypeBox = new JComboBox<AveragingType>(AveragingType.values());
		averagingTypeBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				methodCardLayout.show(averagingMethodPanel, averagingTypeBox.getSelectedItem().toString());
				refreshAllResults();
				updateDisplay();
			}
		});
		averagingTypePanel.add(averagingTypeBox);
		northPanel.add(averagingTypePanel, BorderLayout.NORTH);

		methodCardLayout = new CardLayout();
		averagingMethodPanel = new JPanel(methodCardLayout);	
		northPanel.add(averagingMethodPanel, BorderLayout.CENTER);

		averagingDiskPanel = new JPanel(new GridLayout(2, 2));
		averagingDiskPanel.add(new JLabel("Diameter of the disk area in pixels"));
		JSpinner diskDiameterSpinner = new JSpinner(diskDiameterModel);
		diskDiameterSpinner.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				//				updatePositions();
				refreshAllResults();
			}});
		averagingDiskPanel.add(diskDiameterSpinner);
		averagingDiskPanel.add(new JLabel("Criterion to display"));
		averagingDiskPanel.add(criteriaBox);
		criteriaBox.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				updateDisplay();
			}
		});
		averagingMethodPanel.add(averagingDiskPanel, AveragingType.DISK.toString());

		averagingDiskNoBackgroundPanel = new JPanel(new GridLayout(4, 2));
		averagingDiskNoBackgroundPanel.add(new JLabel("Diameter of the disk area in pixels"));
		JSpinner diskDiameterSpinner2 = new JSpinner(diskDiameterModel);
		diskDiameterSpinner2.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				refreshAllResults();
			}});
		averagingDiskNoBackgroundPanel.add(diskDiameterSpinner2);
		JSpinner diskOutterDiameterSpinner2 = new JSpinner(diskOutterDiameterModel);
		diskOutterDiameterSpinner2.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				refreshAllResults();
			}});
		averagingDiskNoBackgroundPanel.add(new JLabel("Diameter of the background disk area in pixels"));
		averagingDiskNoBackgroundPanel.add(diskOutterDiameterSpinner2);
		averagingDiskNoBackgroundPanel.add(new JLabel("Criterion to display"));
		averagingDiskNoBackgroundPanel.add(criteriaBox2);
		criteriaBox2.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				updateDisplay();
			}
		});
		averagingMethodPanel.add(averagingDiskNoBackgroundPanel, AveragingType.DISK_BACKGROUND_CORRECTED.toString());


		averagingMaskPanel = new JPanel(new GridLayout(2, 2));
		averagingMaskPanel.add(new JLabel("Missing detections masks replaced by value at center of mass."));
		averagingMaskPanel.add(new JLabel(""));
		averagingMaskPanel.add(new JLabel("Criterion to display"));
		averagingMaskPanel.add(criteriaBox3);
		criteriaBox3.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				updateDisplay();
			}
		});
		averagingMethodPanel.add(averagingMaskPanel, AveragingType.SPOT_MASK.toString());


		averagingGaussianFitPanel = new JPanel(new GridLayout(2, 2));
		averagingGaussianFitPanel.add(new JLabel("Standard deviation"));
		standardDeviationTF = new JTextField("1.5", 5);
		standardDeviationTF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateDisplay();				
			}
		});
		averagingGaussianFitPanel.add(standardDeviationTF);
		averagingGaussianFitPanel.add(new JLabel("Criterion to display"));
		averagingGaussianFitPanel.add(criteriaGaussianBox);
		criteriaGaussianBox.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				updateDisplay();
			}
		});
		averagingMethodPanel.add(averagingGaussianFitPanel, AveragingType.GAUSSIAN_FIT2D.toString());		

		northPanel.add(displayLegendBox, BorderLayout.SOUTH);
		displayLegendBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateDisplay();
			};
		});

		this.panel.add(northPanel, BorderLayout.NORTH);

		JScrollPane centerPane = new JScrollPane(chartsPanel);
		this.panel.add(centerPane, BorderLayout.CENTER);

		JPanel southPane = new JPanel();
		southPane.setLayout(new BorderLayout());
		JButton exportResultsButton = new JButton("Export results");
		exportResultsButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				exportResults();
			}
		});
		southPane.add(exportResultsButton);

		this.panel.add(southPane, BorderLayout.SOUTH);

		fillSeriesThread = new FillSeriesThread();
		fillSeriesThread.start();

		changeSelectedSequence(getActiveSequence());
	}

	public void exportResults()
	{
		ArrayList<TrackAnalysis> analyzedTracks = new ArrayList<TrackAnalysis>();
		try{
			seriesToFillLock.lock();
			analyzedTracks.addAll(this.analyzedTracks);
		}
		finally{
			seriesToFillLock.unlock();
		}
		Object[] possibilities = {"Excel (xls)", "Text (txt)"};
		String s = (String)JOptionPane.showInputDialog(
				this.getPanel(), "Select the output format",
				"Intensity output",
				JOptionPane.PLAIN_MESSAGE,
				null,
				possibilities,
				"Excel (xls)");
		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)){
			if (s.equals(("Excel (xls)")))
				exportToXLS(analyzedTracks, (AveragingType)averagingTypeBox.getSelectedItem());
			else
				exportToTXT(analyzedTracks, (AveragingType)averagingTypeBox.getSelectedItem());				
		}
	}


	private void exportToTXT(final ArrayList<TrackAnalysis> trackList, final AveragingType averType)
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Choose txt file.");
		int returnVal = chooser.showOpenDialog(this.panel);
		if(returnVal != JFileChooser.APPROVE_OPTION) return;

		File file = chooser.getSelectedFile();
		if (file == null)
			return;
		if (!file.getName().endsWith(".txt"))
		{
			try {
				file = new File(file.getCanonicalPath()+".txt");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		final ArrayList<TrackAnalysis> tList = new ArrayList<TrackAnalysis>();
		for (TrackAnalysis ta:trackList)
			if (ta.averagingType == averType)
				tList.add(ta);
		final int numChannels = computeMaxNumChannels(tList);
		final File[] outputFiles = new File[numChannels];
		if (numChannels > 1)
			try {
				for (int c = 0; c < numChannels; c++)
					outputFiles[c] = new File(file.getCanonicalPath().substring(0, file.getCanonicalPath().toCharArray().length - 4)+"_"+c+".txt");
			} catch (IOException e) {
				e.printStackTrace();
			}
		else
			outputFiles[0] = file;
		File startIdxFile = null;
		try {
			startIdxFile = new File(file.getCanonicalPath().substring(0, file.getCanonicalPath().toCharArray().length - 4)+"_trackStartIndex.txt");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		final File startFile = startIdxFile;

		if (tList.size() < 1)
			return;
		Thread savingThread = new Thread()
		{
			@Override
			public void run()
			{
				BufferedWriter[] writers = new BufferedWriter[numChannels];
				try {
					for (int i = 0; i < numChannels; i++)
						writers[i] = new BufferedWriter(new FileWriter(outputFiles[i]));
				} catch (IOException e) {
					e.printStackTrace();
				}

				AnnounceFrame announce1 = new AnnounceFrame("Saving results");
				ArrayList<Integer> startIdx = new ArrayList<Integer>();
				switch (averType)
				{
				case DISK:
					try {
						// append tracks one after another in the same file
						// then create a file for indices of start and end of tracks
						int k = 0;
						for (TrackAnalysis ta:tList)
						{
							startIdx.add(new Integer(k));
							TrackAnalysisDiskAveraging taa = (TrackAnalysisDiskAveraging) ta;
							for (int c = 0; c < numChannels; c++)
							{
								for (int i = 0; i < taa.meanIntensity[c].getItemCount(); i++)
								{
									int t = taa.meanIntensity[c].getX(i).intValue();
									double mean = taa.meanIntensity[c].getY(i).doubleValue();
									double max = taa.maxIntensity[c].getY(i).doubleValue();
									double min = taa.minIntensity[c].getY(i).doubleValue();
									double var = taa.varIntensity[c].getY(i).doubleValue();
									double median = taa.medianIntensity[c].getY(i).doubleValue();
									writers[c].write(t + ", "+ mean + ", " + min + ", " + max + ", " + var + ", " + median + "\n");
									if (c == 0)
										k++;
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				case SPOT_MASK:
					try {
						// append tracks one after another in the same file
						// then create a file for indices of start and end of tracks
						int k = 0;
						for (TrackAnalysis ta:tList)
						{
							startIdx.add(new Integer(k));
							TrackAnalysisMaskAveraging taa = (TrackAnalysisMaskAveraging) ta;
							for (int c = 0; c < numChannels; c++)
							{
								for (int i = 0; i < taa.meanIntensity[c].getItemCount(); i++)
								{
									int t = taa.meanIntensity[c].getX(i).intValue();
									double mean = taa.meanIntensity[c].getY(i).doubleValue();
									double max = taa.maxIntensity[c].getY(i).doubleValue();
									double min = taa.minIntensity[c].getY(i).doubleValue();
									double var = taa.varIntensity[c].getY(i).doubleValue();
									double median = taa.medianIntensity[c].getY(i).doubleValue();
									writers[c].write(t + ", "+ mean + ", " + min + ", " + max + ", " + var + ", " + median + "\n");
									if (c == 0)
										k++;
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				case GAUSSIAN_FIT2D:
					try {
						// append tracks one after another in the same file
						// then create a file for indices of start and end of tracks
						int k = 0;
						for (TrackAnalysis ta:tList)
						{
							startIdx.add(new Integer(k));
							TrackAnalysisGaussianFit taa = (TrackAnalysisGaussianFit) ta;
							for (int c = 0; c < numChannels; c++)
							{
								for (int i = 0; i < taa.gaussianAmplitude[c].getItemCount(); i++)
								{
									int t = taa.gaussianAmplitude[c].getX(i).intValue();
									double amplitude = taa.gaussianAmplitude[c].getY(i).doubleValue();
									double background = taa.gaussianAmplitude[c].getY(i).doubleValue();
									writers[c].write(t+", "+amplitude+", " + background + "\n");
									if (c == 0)
										k++;
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				case DISK_BACKGROUND_CORRECTED:
					try {
						// append tracks one after another in the same file
						// then create a file for indices of start and end of tracks
						int k = 0;
						for (TrackAnalysis ta:tList)
						{
							startIdx.add(new Integer(k));
							TrackAnalysisDiskAveragingNoBackground taa = (TrackAnalysisDiskAveragingNoBackground) ta;
							for (int c = 0; c < numChannels; c++)
							{
								for (int i = 0; i < taa.meanIntensity[c].getItemCount(); i++)
								{
									int t = taa.meanIntensity[c].getX(i).intValue();
									double mean = taa.meanIntensity[c].getY(i).doubleValue();
									double max = taa.maxIntensity[c].getY(i).doubleValue();
									double min = taa.minIntensity[c].getY(i).doubleValue();
									double var = taa.varIntensity[c].getY(i).doubleValue();
									double median = taa.medianIntensity[c].getY(i).doubleValue();
									writers[c].write(t + ", "+ mean + ", " + min + ", " + max + ", " + var + ", " + median + "\n");
									if (c == 0)
										k++;
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				default:
					break;				
				}
				try {
					for (BufferedWriter bwr:writers)
						bwr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				BufferedWriter bwr;
				try {
					bwr = new BufferedWriter(new FileWriter(startFile));
					for (Integer it:startIdx)
						bwr.write(it.toString()+"\n");
					bwr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				savingThreadList.remove(this);
				announce1.close();
				new AnnounceFrame("Results saved");
			}
		};
		savingThreadList.add(savingThread);
		savingThread.start();
	}


	private void exportToXLS(ArrayList<TrackAnalysis> trackAnalysisList, final AveragingType averType )
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Choose xls file.");
		//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = chooser.showOpenDialog(this.panel);
		if(returnVal != JFileChooser.APPROVE_OPTION) return;

		File file = chooser.getSelectedFile();
		if (file == null)
			return;
		if (!file.getName().endsWith(".xls"))
		{
			try {	
				file = new File(file.getCanonicalPath()+".xls");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		final ArrayList<TrackAnalysis> tList = new ArrayList<TrackAnalysis>();
		for (TrackAnalysis ta:trackAnalysisList)
			if (ta.averagingType == averType)
				tList.add(ta);
		if (tList.size() < 1)
			return;
		final File XLSFile = file;

		Thread savingThread = new Thread()
		{
			@Override
			public void run()
			{
				AnnounceFrame announce1 = new AnnounceFrame("Saving results");
				// save disk averaging results first

				// check that the number of tracks to save does not exceed the number of allowed columns in the Excel file: 255
				// otherwise, create multiple files
				ArrayList<ArrayList<TrackAnalysis>> tsListList = new ArrayList<ArrayList<TrackAnalysis>>();
				int maxNumCol = 255;
				int numFiles = (int) Math.floor(tList.size()/(double)maxNumCol) + 1;
				if (numFiles > 1)
				{
					for (int k = 0; k < numFiles - 1; k++)
					{
						ArrayList<TrackAnalysis> trkList = new ArrayList<TrackAnalysis>();
						for (int i = 0; i < maxNumCol; i++)
							trkList.add(tList.get(i + k*maxNumCol));
						tsListList.add(trkList);
					}
					ArrayList<TrackAnalysis> trkList = new ArrayList<TrackAnalysis>();
					for (int i = 0; i < (tList.size() - maxNumCol*(numFiles - 1)); i++)
						trkList.add(tList.get(i + maxNumCol*(numFiles - 1)));
					tsListList.add(trkList);
				}
				else
				{
					tsListList.add(tList);
				}
				for (int k = 0; k < numFiles; k++)
				{
					if (k >= tsListList.size())
						break;
					ArrayList<TrackAnalysis> trackList = tsListList.get(k);

					//					XlsManager xls = null;
					//					try {
					//						if (numFiles < 2)
					//							xls = new XlsManager( XLSFile );
					//						else 
					//						{
					//							String filePath = XLSFile.getCanonicalPath();
					//							File XLSFile2 = new File(filePath.substring(0, filePath.toCharArray().length - 4)+"_"+k+".xls");
					//							xls = new XlsManager( XLSFile2 );		
					//						}
					//					} catch (IOException e) {
					//
					//						MessageDialog.showDialog( "Cannot open file." , MessageDialog.ERROR_MESSAGE );
					//						return;
					//					}
					WritableWorkbook workbook = null;
					try {
						if (numFiles < 2)
							workbook = XLSUtil.createWorkbook(XLSFile);
						//							xls = new XlsManager( XLSFile );
						else 
						{
							String filePath = XLSFile.getCanonicalPath();
							File XLSFile2 = new File(filePath.substring(0, filePath.toCharArray().length - 4)+"_"+k+".xls");
							//							xls = new XlsManager( XLSFile2 );
							workbook = XLSUtil.createWorkbook(XLSFile2);							
						}
					} catch (IOException e) {

						MessageDialog.showDialog( "Cannot open file." , MessageDialog.ERROR_MESSAGE );
						return;
					}

					int numChannels = computeMaxNumChannels(trackList);
					switch(averType)
					{
					case DISK:
					{
						int s = 0;
						for (int c = 0; c < numChannels; c++)
						{
							for (int i = 0; i < 6; i++)
							{
								WritableSheet sheet = workbook.createSheet("Channel " + c + " - " + criteria[i], s);
								s++;
								// xls.createNewPage("Channel "+c+" - "+criteria[i]);
								// create the time axis
								int maxT = 0;
								int minT = Integer.MAX_VALUE;
								for (TrackAnalysis trkAnalysis:trackList)
								{
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										if (trkAnalysis.track.getLastDetection().getT() > maxT)
											maxT = trkAnalysis.track.getLastDetection().getT();
										if (trkAnalysis.track.getFirstDetection().getT() < minT)
											minT = trkAnalysis.track.getFirstDetection().getT();
									}
								}
								XLSUtil.setCellString(sheet, 0, 0, "Frame number");
								for (int t = minT; t <= maxT; t++)
									XLSUtil.setCellNumber(sheet, 0, t - minT + 1, t);
								//								xls.setLabel(0, 0, "Frame number");
								//								for (int t = minT; t <= maxT; t++)
								//									xls.setNumber(0, t-minT+1, t);
								int cntTrack = 0;
								for (TrackAnalysis trka:trackList)
								{
									TrackAnalysisDiskAveraging trkAnalysis  = (TrackAnalysisDiskAveraging) trka;
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										cntTrack++;
										//										xls.setLabel(cntTrack, 0, trkAnalysis.description);
										XLSUtil.setCellString(sheet, cntTrack, 0, trkAnalysis.description);
										int firstT = trkAnalysis.track.getFirstDetection().getT();
										switch (i)
										{
										case 0:
										{
											if (trkAnalysis.meanIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.meanIntensity[c].getItemCount(); j++)
												{
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.meanIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.meanIntensity[c].getY(j).doubleValue());
												}
											break;
										}
										case 1:
											if (trkAnalysis.maxIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.maxIntensity[c].getItemCount(); j++)
												{
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.maxIntensity[c].getY(j).doubleValue());s
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.maxIntensity[c].getY(j).doubleValue());
												}
											break;
										case 2:
											if (trkAnalysis.minIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.minIntensity[c].getItemCount(); j++)
												{
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.minIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.minIntensity[c].getY(j).doubleValue());
												}
											break;
										case 3:
											if (trkAnalysis.medianIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.medianIntensity[c].getItemCount(); j++)
												{
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.medianIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.medianIntensity[c].getY(j).doubleValue());
												}
											break;
										case 4:
											if (trkAnalysis.sumIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.sumIntensity[c].getItemCount(); j++)
												{
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.sumIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.sumIntensity[c].getY(j).doubleValue());
												}
											break;
										case 5:
											if (trkAnalysis.varIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.varIntensity[c].getItemCount(); j++)
												{
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.varIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.varIntensity[c].getY(j).doubleValue());
												}
											break;
										}
									}
								}
							}
						}
						break;
					}
					case SPOT_MASK:
					{
						for (int c = 0; c < numChannels; c++)
						{
							for (int i = 0; i < 6; i++)
							{
								//								xls.createNewPage("Channel "+c+" - "+criteria[i]);
								WritableSheet sheet = XLSUtil.createNewPage(workbook, "Channel "+c+" - "+criteria[i]);

								// create the time axis
								int maxT = 0;
								int minT = Integer.MAX_VALUE;
								for (TrackAnalysis trkAnalysis:trackList)
								{
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										if (trkAnalysis.track.getLastDetection().getT() > maxT)
											maxT = trkAnalysis.track.getLastDetection().getT();
										if (trkAnalysis.track.getFirstDetection().getT() < minT)
											minT = trkAnalysis.track.getFirstDetection().getT();
									}
								}
								XLSUtil.setCellString(sheet, 0, 0, "Frame number");
								//								xls.setLabel(0, 0, "Frame number");
								for (int t = minT; t <= maxT; t++)
									XLSUtil.setCellNumber(sheet, 0, t - minT + 1, t);
								//									xls.setNumber(0, t - minT + 1, t);
								int cntTrack = 0;
								for (TrackAnalysis trka:trackList)
								{
									TrackAnalysisMaskAveraging trkAnalysis  = (TrackAnalysisMaskAveraging) trka;
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										cntTrack++;
										//										xls.setLabel(cntTrack, 0, trkAnalysis.description);
										XLSUtil.setCellString(sheet, cntTrack, 0, trkAnalysis.description);
										int firstT = trkAnalysis.track.getFirstDetection().getT();
										switch (i)
										{
										case 0:
										{
											if (trkAnalysis.meanIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.meanIntensity[c].getItemCount(); j++)
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.meanIntensity[c].getY(j).doubleValue());
											//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.meanIntensity[c].getY(j).doubleValue());
											break;
										}
										case 1:
											if (trkAnalysis.maxIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.maxIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.maxIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.maxIntensity[c].getY(j).doubleValue());
											break;
										case 2:
											if (trkAnalysis.minIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.minIntensity[c].getItemCount(); j++)
													XLSUtil.setCellNumber(sheet, cntTrack, firstT + j + 1, trkAnalysis.minIntensity[c].getY(j).doubleValue());
											//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.minIntensity[c].getY(j).doubleValue());
											break;
										case 3:
											if (trkAnalysis.medianIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.medianIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.medianIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.medianIntensity[c].getY(j).doubleValue());
											break;
										case 4:
											if (trkAnalysis.sumIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.sumIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.sumIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.sumIntensity[c].getY(j).doubleValue());
											break;
										case 5:
											if (trkAnalysis.varIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.varIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.varIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.varIntensity[c].getY(j).doubleValue());
											break;
										}
									}
								}
							}
						}
						break;
					}
					case GAUSSIAN_FIT2D:
						for (int c = 0; c < numChannels; c++)
						{
							for (int i = 0; i < 2; i++)
							{
								//								xls.createNewPage("Channel "+c+" - "+criteriaGaussian[i]);
								WritableSheet sheet = XLSUtil.createNewPage(workbook, "Channel "+c+" - "+criteriaGaussian[i]);
								// create the time axis
								int maxT = 0;
								int minT = Integer.MAX_VALUE;
								for (TrackAnalysis trkAnalysis:trackList)
								{
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										if (trkAnalysis.track.getLastDetection().getT() > maxT)
											maxT = trkAnalysis.track.getLastDetection().getT();
										if (trkAnalysis.track.getFirstDetection().getT() < minT)
											minT = trkAnalysis.track.getFirstDetection().getT();
									}
								}
								XLSUtil.setCellString(sheet, 0, 0, "Frame number");
								//								xls.setLabel(0, 0, "Frame number");
								for (int t = minT; t <= maxT; t++)
									//									xls.setNumber(0, t-minT+1, t);
									XLSUtil.setCellNumber(sheet, 0, t - minT + 1, t);
								int cntTrack = 0;
								for (TrackAnalysis trka:trackList)
								{
									TrackAnalysisGaussianFit trkAnalysis  = (TrackAnalysisGaussianFit) trka;
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										cntTrack++;
										//										xls.setLabel(cntTrack, 0, trkAnalysis.description);
										XLSUtil.setCellString(sheet, cntTrack, 0, trkAnalysis.description);
										int firstT = trkAnalysis.track.getFirstDetection().getT();
										switch (i)
										{
										case 0:
										{
											if (trkAnalysis.gaussianAmplitude.length >= c)
												for (int j = 0; j < trkAnalysis.gaussianAmplitude[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.gaussianAmplitude[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT + j + 1, trkAnalysis.gaussianAmplitude[c].getY(j).doubleValue());
											break;
										}
										case 1:
											if (trkAnalysis.gaussianBackground.length >= c)
												for (int j = 0; j < trkAnalysis.gaussianBackground[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.gaussianBackground[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT + j + 1, trkAnalysis.gaussianBackground[c].getY(j).doubleValue());
											break;
										}
									}
								}
							}
						}
						break;
					case DISK_BACKGROUND_CORRECTED:
						for (int c = 0; c < numChannels; c++)
						{
							for (int i = 0; i < 6; i++)
							{
								WritableSheet sheet = XLSUtil.createNewPage(workbook, "Channel "+c+" - "+criteria[i]);
								//								xls.createNewPage("Channel "+c+" - "+criteria[i]);
								// create the time axis
								int maxT = 0;
								int minT = Integer.MAX_VALUE;
								for (TrackAnalysis trkAnalysis:trackList)
								{
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										if (trkAnalysis.track.getLastDetection().getT() > maxT)
											maxT = trkAnalysis.track.getLastDetection().getT();
										if (trkAnalysis.track.getFirstDetection().getT() < minT)
											minT = trkAnalysis.track.getFirstDetection().getT();
									}
								}
								XLSUtil.setCellString(sheet, 0, 0, "Frame number");
								//								xls.setLabel(0, 0, "Frame number");
								for (int t = minT; t <= maxT; t++)
									//									xls.setNumber(0, t-minT+1, t);
									XLSUtil.setCellNumber(sheet, 0, t - minT + 1, t);
								int cntTrack = 0;
								for (TrackAnalysis trka:trackList)
								{
									TrackAnalysisDiskAveragingNoBackground trkAnalysis  = (TrackAnalysisDiskAveragingNoBackground) trka;
									if (!trkAnalysis.track.getDetectionList().isEmpty())
									{
										cntTrack++;
										//										xls.setLabel(cntTrack, 0, trkAnalysis.description);
										XLSUtil.setCellString(sheet, cntTrack, 0, trkAnalysis.description);										
										int firstT = trkAnalysis.track.getFirstDetection().getT();
										switch (i)
										{
										case 0:
										{
											if (trkAnalysis.meanIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.meanIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.meanIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.meanIntensity[c].getY(j).doubleValue());
											break;
										}
										case 1:
											if (trkAnalysis.maxIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.maxIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.maxIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.maxIntensity[c].getY(j).doubleValue());
											break;
										case 2:
											if (trkAnalysis.minIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.minIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.minIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.minIntensity[c].getY(j).doubleValue());
											break;
										case 3:
											if (trkAnalysis.medianIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.medianIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.medianIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.medianIntensity[c].getY(j).doubleValue());
											break;
										case 4:
											if (trkAnalysis.sumIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.sumIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.sumIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.sumIntensity[c].getY(j).doubleValue());
											break;
										case 5:
											if (trkAnalysis.varIntensity.length >= c)
												for (int j = 0; j < trkAnalysis.varIntensity[c].getItemCount(); j++)
													//													xls.setNumber(cntTrack, firstT+j+1, trkAnalysis.varIntensity[c].getY(j).doubleValue());
													XLSUtil.setCellNumber(sheet, cntTrack, firstT+j+1, trkAnalysis.varIntensity[c].getY(j).doubleValue());
											break;
										}
									}
								}
							}
						}
						break;
					default:
						break;
					}
					try {
						XLSUtil.saveAndClose(workbook);
					} catch (WriteException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
//					xls.SaveAndClose();
				}
				savingThreadList.remove(this);
				announce1.close();
				new AnnounceFrame("Results saved");
			}
		};
		savingThreadList.add(savingThread);
		savingThread.start();
	}

	private ArrayList<Thread> savingThreadList = new ArrayList<Thread>();


	@Override
	public void Close() {
		savingThreadList.clear();
		analyzeThreadList.clear();
	}

	ArrayList<Thread> analyzeThreadList = new ArrayList<Thread>();

	@Override
	public void Compute() {
		if( ! isEnabled() ) return;
		Sequence sequence = this.trackPool.getDisplaySequence();
		if (sequence != selectedSequence)
		{
			changeSelectedSequence(sequence);
			return;
		}
		final AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();
		Thread analyzeThread = new Thread()
		{		
			@Override
			public void run()
			{
				// then performAnalysis in separate Thread
				try{
					seriesToFillLock.lock();
					//check whether some tracks were removed
					boolean notFound = false;
					for (TrackAnalysis ta:analyzedTracks)
						if (!trackPool.getTrackSegmentList().contains(ta.track))
						{
							notFound = true;
							break;
						}
					// if missing tracks, recompute everything as labels have changed for tracks
					if (notFound)
						analyzedTracks.clear();

					// get the tracks to Analyze
					ArrayList<TrackSegment> toAnalyzeTracks = getTracksToAnalyze(averagingMethod);

					for (TrackSegment ts:toAnalyzeTracks)
					{
						if (!seriesToFill.contains(ts))
							seriesToFill.add(ts);
					}
					seriesToFillCondition.signalAll();
				}
				finally
				{
					seriesToFillLock.unlock();
					analyzeThreadList.remove(this);
				}
			}
		};
		analyzeThreadList.add(analyzeThread);
		analyzeThread.start();
	}

	private void changeSelectedSequence(Sequence selectedSequence)
	{
		if (selectedSequence != this.selectedSequence)
		{
			//			if (this.selectedSequence!=null)
			//				this.selectedSequence.removePainter(this);
			this.selectedSequence = selectedSequence;
			//			this.selectedSequence.addPainter(this);
			refreshAllResults();
		}
	}

	public ArrayList<TrackSegment> getTracksToAnalyze(AveragingType averagingMethod)
	{
		//check whether the enabled tracks have already been analyzed
		ArrayList<TrackSegment> enabledTracks = new ArrayList<TrackSegment>();
		ArrayList<TrackSegment> copyTracks = new ArrayList<TrackSegment>();
		if (trackPool != null)
			copyTracks.addAll(trackPool.getTrackSegmentList());
		for (TrackSegment track:copyTracks)
		{
			boolean enabled = false;
			for (Detection d:track.getDetectionList())
				if (d.isEnabled())
				{
					enabled = true;
					break;
				}
			if (enabled)
				enabledTracks.add(track);
		}
		ArrayList<TrackSegment> toAnalyzeTracks = new ArrayList<TrackSegment>();
		for (TrackSegment ts:enabledTracks)
		{
			boolean found = false;
			for (TrackAnalysis analysis:analyzedTracks)
			{
				if (analysis.track == ts && analysis.averagingType == averagingMethod)
				{
					found = true;
					break;
				}
			}
			if (!found)
				toAnalyzeTracks.add(ts);
		}
		return toAnalyzeTracks;
	}

	@Override
	public void displaySequenceChanged()
	{
		changeSelectedSequence(getActiveSequence());
	}

	public void refreshAllResults()
	{
		try{
			AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();
			seriesToFillLock.lock();
			analyzedTracks.clear();
			seriesToFill.clear();
			seriesToFill.addAll(getTracksToAnalyze(averagingMethod));
			seriesToFillCondition.signalAll();
		}
		finally{
			seriesToFillLock.unlock();
		}		
	}

	class FillSeriesThread extends Thread
	{
		boolean stop = false;

		public FillSeriesThread(){}

		@Override
		public void run()
		{
			while (!stop)
			{
				try{
					seriesToFillLock.lock();
					if (seriesToFill.isEmpty())
						seriesToFillCondition.await();
					if (selectedSequence != null)
					{
						double stdGaussian = Double.parseDouble(standardDeviationTF.getText());
						double diskRadius = ((Number)(diskDiameterModel.getValue())).doubleValue();
						double diskOutterRadius = ((Number)(diskOutterDiameterModel.getValue())).doubleValue();


						AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();

						fillSeries(seriesToFill, selectedSequence, stdGaussian, diskRadius, diskOutterRadius, averagingMethod);//TODO: should be outside of the lock
					}
					//TODO: if outside the lock, some new task may come, while already being processed
					seriesToFill.clear();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				finally
				{
					seriesToFillLock.unlock();
				}
				SwingUtilities.invokeLater(new Runnable(){public void run(){updateDisplay();}});
			}
		}
	}


	public void updateDisplay()
	{
		ArrayList<TrackAnalysis> analyzedTrkList = new ArrayList<TrackAnalysis>();
		try{
			analyzedTracksLock.lock();
			analyzedTrkList.addAll(analyzedTracks);
		}
		finally
		{
			analyzedTracksLock.unlock();
		}
		updateCharts(analyzedTrkList, (AveragingType)averagingTypeBox.getSelectedItem());
	}

	private int computeMaxNumChannels(ArrayList<TrackAnalysis> analyzedTrkList)
	{
		int maxNumChannels = 0;
		for (TrackAnalysis tr:analyzedTrkList)
			if (tr.sequence.getSizeC() > maxNumChannels)
				maxNumChannels = tr.sequence.getSizeC();
		return maxNumChannels;
	}

	private void updateCharts(ArrayList<TrackAnalysis> analyzedTrkList, AveragingType averagingMethod)
	{
		//	displayedTracks.clear();
		int maxNumChannels = computeMaxNumChannels(analyzedTrkList);
		chartsPanel.removeAll();
		if (maxNumChannels > 0)
		{
			chartsPanel.setLayout(new GridLayout(maxNumChannels, 1));
		}
		else
			return;
		for (int c = 0; c < maxNumChannels; c++)
		{
			XYSeriesCollection xyDataset = new XYSeriesCollection();
			for (TrackAnalysis tr:analyzedTrkList)
			{
				if (averagingMethod == tr.averagingType)
				{
					int firstT = -1;
					int lastT = -1;
					boolean nonEmpty = false;
					if (tr.track.getDetectionList().size() == 1)
					{
						if (tr.track.getDetectionList().get(0).isEnabled())
						{
							firstT = tr.track.getDetectionList().get(0).getT();
							lastT = firstT;
							nonEmpty = true;
						}
					}
					else
					{
						for (int t = tr.track.getFirstDetection().getT(); t <= tr.track.getLastDetection().getT(); t++)
							if (tr.track.getDetectionAtTime(t).isEnabled())
							{
								firstT = t;
								lastT = t;
								nonEmpty = true;
								break;
							}
						if (nonEmpty)
							for (int t = firstT+1; t <= tr.track.getLastDetection().getT(); t++)
								if (tr.track.getDetectionAtTime(t).isEnabled())
									lastT = t;
					}
					if (nonEmpty)
					{
						//if (!displayedTracks.contains(tr.track))
						//	displayedTracks.add(tr);
						firstT = firstT - tr.track.getFirstDetection().getT();
						lastT = lastT - tr.track.getFirstDetection().getT();
						try {
							switch (tr.averagingType)
							{
							case DISK:
							{
								XYSeries cropSeries = null;
								switch (criteriaBox.getSelectedIndex())
								{
								case 0:
									cropSeries = ((TrackAnalysisDiskAveraging)tr).meanIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 1:
									cropSeries =  ((TrackAnalysisDiskAveraging)tr).maxIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 2:
									cropSeries = ((TrackAnalysisDiskAveraging)tr).minIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 3:
									cropSeries = ((TrackAnalysisDiskAveraging)tr).medianIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 4:
									cropSeries = ((TrackAnalysisDiskAveraging)tr).sumIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 5:
									cropSeries = ((TrackAnalysisDiskAveraging)tr).varIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								default:
									System.out.println("not found series");
								}
								xyDataset.addSeries(cropSeries);
								break;
							}
							case SPOT_MASK:
							{
								XYSeries cropSeries = null;
								switch (criteriaBox3.getSelectedIndex())
								{
								case 0:
									cropSeries = ((TrackAnalysisMaskAveraging)tr).meanIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 1:
									cropSeries =  ((TrackAnalysisMaskAveraging)tr).maxIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 2:
									cropSeries = ((TrackAnalysisMaskAveraging)tr).minIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 3:
									cropSeries = ((TrackAnalysisMaskAveraging)tr).medianIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 4:
									cropSeries = ((TrackAnalysisMaskAveraging)tr).sumIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 5:
									cropSeries = ((TrackAnalysisMaskAveraging)tr).varIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								default:
									System.out.println("not found series");
								}
								xyDataset.addSeries(cropSeries);
								break;
							}
							case GAUSSIAN_FIT2D:
							{
								XYSeries cropSeries = null;
								switch (criteriaGaussianBox.getSelectedIndex())
								{
								case 0:
									cropSeries = ((TrackAnalysisGaussianFit)tr).gaussianAmplitude[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 1:
									cropSeries = ((TrackAnalysisGaussianFit)tr).gaussianBackground[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);								
									break;
								}
								xyDataset.addSeries(cropSeries);
								break;
							}
							case DISK_BACKGROUND_CORRECTED:
							{

								XYSeries cropSeries = null;
								switch (criteriaBox2.getSelectedIndex())
								{
								case 0:
									cropSeries = ((TrackAnalysisDiskAveragingNoBackground)tr).meanIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 1:
									cropSeries =  ((TrackAnalysisDiskAveragingNoBackground)tr).maxIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 2:
									cropSeries = ((TrackAnalysisDiskAveragingNoBackground)tr).minIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 3:
									cropSeries = ((TrackAnalysisDiskAveragingNoBackground)tr).medianIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 4:
									cropSeries = ((TrackAnalysisDiskAveragingNoBackground)tr).sumIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								case 5:
									cropSeries = ((TrackAnalysisDiskAveragingNoBackground)tr).varIntensity[c].createCopy(firstT, lastT);
									cropSeries.setDescription(tr.description);
									break;
								default:
									System.out.println("not found series");
								}
								xyDataset.addSeries(cropSeries);
								break;
							}
							default:
								break;
							}
						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
						}
					}
				}
			}

			String TitleString = "Channel "+c;
			String TitleString2 = "Frame";
			String TitleString3 = "Intensity criterion";
			boolean displayLegend = displayLegendBox.isSelected();

			JFreeChart chart = ChartFactory.createXYLineChart(
					TitleString,
					TitleString2,
					TitleString3,
					xyDataset,
					PlotOrientation.VERTICAL,
					displayLegend,
					true,
					false
					);
			int minWidth = 300;
			int minHeight = 200;
			int width = 300;
			int height = 200;
			int maxWidth = 10000;
			int maxHeight = 10000;
			chartsPanel.add( new ChartPanel(  chart , width , height , minWidth, minHeight, maxWidth , maxHeight, false , false, true , true , true, true));		
		}
		this.chartsPanel.validate();
	}

	public void fillSeries(ArrayList<TrackSegment> tracks, Sequence sequence, double stdGaussian, double diskRadius, double outterDiskRadius, final AveragingType averagingMethod)
	{
		// discriminate between new series and existing series to complement

		final ArrayList<TrackAnalysis> analyzedTrkList = new ArrayList<TrackAnalysis>();
		boolean[] frameHasDetection = new boolean[sequence.getSizeT()];
		switch (averagingMethod) {
		case DISK:
		{
			ArrayList<TrackAnalysisDiskAveraging> analysisList = new ArrayList<TrackAnalysisDiskAveraging>();
			for (TrackSegment ts:tracks)
			{
				TrackGroup group = ts.getOwnerTrackGroup();
				TrackAnalysisDiskAveraging analysis;
				if (group == null)
					analysis = new TrackAnalysisDiskAveraging(ts, sequence, "Track "+trackPool.getTrackSegmentList().indexOf(ts), averagingMethod);				
				else
					analysis = new TrackAnalysisDiskAveraging(ts, sequence, group.getDescription()+"#"+group.getTrackSegmentList().indexOf(ts), averagingMethod);
				analysisList.add(analysis);
				analyzedTrkList.add(analysis);
				int t1 = (int) Math.max(ts.getFirstDetection().getT(), 0);
				int t2 = (int) Math.min(ts.getLastDetection().getT(), sequence.getSizeT() - 1);
				for (int t = t1; t <= t2; t++)
					frameHasDetection[t] = true;
			}
			for (int t = 0; t < sequence.getSizeT(); t++)
			{
				if (frameHasDetection[t])
				{
					double[][][] imageTab = new double[sequence.getSizeZ()][sequence.getSizeC()][];
					for (int z = 0; z < sequence.getSizeZ(); z++)
						for (int c = 0; c < sequence.getSizeC(); c++)
							imageTab[z][c] = (double[]) ArrayUtil.arrayToDoubleArray(sequence.getImage(t, z, c).getDataXY(0), sequence.isSignedDataType());
					for (TrackAnalysisDiskAveraging ta:analysisList)
						ta.fillAveragingSeriesAtT(t, imageTab, diskRadius);
				}
			}
			break;
		}
		case SPOT_MASK:
		{
			ArrayList<TrackAnalysisMaskAveraging> analysisList = new ArrayList<TrackAnalysisMaskAveraging>();
			for (TrackSegment ts:tracks)
			{
				TrackGroup group = ts.getOwnerTrackGroup();
				TrackAnalysisMaskAveraging analysis;
				if (group == null)
					analysis = new TrackAnalysisMaskAveraging(ts, sequence, "Track "+trackPool.getTrackSegmentList().indexOf(ts), averagingMethod);				
				else
					analysis = new TrackAnalysisMaskAveraging(ts, sequence, group.getDescription()+"#"+group.getTrackSegmentList().indexOf(ts), averagingMethod);
				analysisList.add(analysis);
				analyzedTrkList.add(analysis);
				int minT = (int) Math.min(ts.getLastDetection().getT(), sequence.getSizeT() - 1);
				for (int t = ts.getFirstDetection().getT(); t <= minT; t++)
					frameHasDetection[t] = true;
			}
			for (int t = 0; t < sequence.getSizeT(); t++)
			{
				if (frameHasDetection[t])
				{
					double[][][] imageTab = new double[sequence.getSizeZ()][sequence.getSizeC()][];
					for (int z = 0; z < sequence.getSizeZ(); z++)
						for (int c = 0; c < sequence.getSizeC(); c++)
							imageTab[z][c] = (double[]) ArrayUtil.arrayToDoubleArray(sequence.getImage(t, z, c).getDataXY(0), sequence.isSignedDataType());
					for (TrackAnalysisMaskAveraging ta:analysisList)
						ta.fillAveragingSeriesAtT(t, imageTab, diskRadius);
				}
			}
			break;
		}
		case GAUSSIAN_FIT2D:
		{
			ArrayList<TrackAnalysisGaussianFit> analysisList = new ArrayList<TrackAnalysisGaussianFit>();
			for (TrackSegment ts:tracks)
			{
				TrackGroup group = ts.getOwnerTrackGroup();
				TrackAnalysisGaussianFit analysis;
				if (group == null)
					analysis = new TrackAnalysisGaussianFit(ts, sequence, "Track "+trackPool.getTrackSegmentList().indexOf(ts), averagingMethod);				
				else
					analysis = new TrackAnalysisGaussianFit(ts, sequence, group.getDescription()+"#"+group.getTrackSegmentList().indexOf(ts), averagingMethod);
				analysisList.add(analysis);
				analyzedTrkList.add(analysis);
				int minT = (int) Math.min(ts.getLastDetection().getT(), sequence.getSizeT() - 1);
				for (int t = ts.getFirstDetection().getT(); t <= minT; t++)
					frameHasDetection[t] = true;
			}
			for (int t = 0; t < sequence.getSizeT(); t++)
			{
				if (frameHasDetection[t])
				{
					double[][] imageTab = new double[sequence.getSizeC()][];
					for (int c = 0; c < sequence.getSizeC(); c++)
						imageTab[c] = (double[]) ArrayUtil.arrayToDoubleArray(sequence.getImage(t, 0, c).getDataXY(0), sequence.isSignedDataType());
					for (TrackAnalysisGaussianFit ta:analysisList)
						ta.fillGaussianFitSeriesAtT(t, imageTab, stdGaussian);
				}
			}
			break;
		}
		case DISK_BACKGROUND_CORRECTED:
		{
			ArrayList<TrackAnalysisDiskAveragingNoBackground> analysisList = new ArrayList<TrackAnalysisDiskAveragingNoBackground>();
			for (TrackSegment ts:tracks)
			{
				TrackGroup group = ts.getOwnerTrackGroup();
				TrackAnalysisDiskAveragingNoBackground analysis;
				if (group == null)
					analysis = new TrackAnalysisDiskAveragingNoBackground(ts, sequence, "Track "+trackPool.getTrackSegmentList().indexOf(ts), averagingMethod);				
				else
					analysis = new TrackAnalysisDiskAveragingNoBackground(ts, sequence, group.getDescription()+"#"+group.getTrackSegmentList().indexOf(ts), averagingMethod);
				analysisList.add(analysis);
				analyzedTrkList.add(analysis);
				for (int t = ts.getFirstDetection().getT(); t <= ts.getLastDetection().getT(); t++)
					frameHasDetection[t] = true;
			}
			for (int t = 0; t < sequence.getSizeT(); t++)
			{
				if (frameHasDetection[t])
				{
					double[][][] imageTab = new double[sequence.getSizeZ()][sequence.getSizeC()][];
					for (int z = 0; z < sequence.getSizeZ(); z++)
						for (int c = 0; c < sequence.getSizeC(); c++)
							imageTab[z][c] = (double[]) ArrayUtil.arrayToDoubleArray(sequence.getImage(t, z, c).getDataXY(0), sequence.isSignedDataType());
					for (TrackAnalysisDiskAveragingNoBackground ta:analysisList)
						ta.fillAveragingSeriesAtT(t, imageTab, diskRadius, outterDiskRadius);
				}
			}
			break;
		}
		default:
			break;
		}

		try{
			analyzedTracksLock.lock();
			analyzedTracks.addAll(analyzedTrkList);
			final ArrayList<TrackAnalysis>  copyAnalyzedTracks = new ArrayList<TrackAnalysis>(analyzedTracks);
			SwingUtilities.invokeLater(new Runnable(){
				@Override
				public void run() {
					updateCharts(copyAnalyzedTracks, averagingMethod);
				}});
		}
		finally
		{
			analyzedTracksLock.unlock();
		}
	}
}
