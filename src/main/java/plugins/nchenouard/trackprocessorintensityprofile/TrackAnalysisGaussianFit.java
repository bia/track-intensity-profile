package plugins.nchenouard.trackprocessorintensityprofile;

import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.collection.array.ArrayUtil;
import icy.type.rectangle.Rectangle5D;

import org.jfree.data.xy.XYSeries;

import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;

public class TrackAnalysisGaussianFit extends TrackAnalysis
{
	XYSeries[] gaussianAmplitude;
	XYSeries[] gaussianBackground;

	TrackAnalysisGaussianFit(TrackSegment track, Sequence sequence,
			String description, AveragingType averagingMethod) {
		super(track, sequence, description, averagingMethod);

		gaussianAmplitude = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			gaussianAmplitude[c] = new XYSeries(description);
		gaussianBackground = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			gaussianBackground[c] = new XYSeries(description);
	}

	@Override
	public void clearSeries() {
		for (int c = 0; c < gaussianAmplitude.length; c++)
			gaussianAmplitude[c].clear();
		for (int c = 0; c < gaussianBackground.length; c++)
			gaussianBackground[c].clear();
	}
	
	public void fillGaussianFitSeriesAtT(int t, double[][] imageTab, double stdGaussian)
	{
		if (track.getFirstDetection().getT()<= t && track.getLastDetection().getT() >= t)
		{
			Detection d = track.getDetectionAtTime(t);
			if (d != null)
			{
				int z = (int)Math.round(d.getZ());
				int width = sequence.getSizeX();
				int height = sequence.getSizeY();		
				// compute the Gaussian amplitude				
				int minX = (int) d.getX() - halfCropSize;
				double pCropX = halfCropSize + (d.getX() - (int) d.getX());
				if (minX < 0)
				{
					pCropX -= minX;
					minX = 0;
				}
				int minY =  (int) d.getY() - halfCropSize;
				double pCropY = halfCropSize + (d.getY() - (int) d.getY());
				if (minY < 0)
				{
					pCropY -= minY;
					minY = 0;
				}
				int maxX = (int) Math.min(Math.ceil(d.getX()) + halfCropSize, width - 1);
				int maxY = (int) Math.min(Math.ceil(d.getY()) + halfCropSize, height - 1);
				int cropWidth = maxX - minX + 1;
				int cropHeight = maxY - minY + 1;
				
				for (int c = 0; c < sequence.getSizeC(); c++)
				{
					// fit the model background + amplitude*GaussianProfile to the cropped image around the detected position
					//					Sequence crop = SequenceUtil.getSubSequence(sequence, minX, minY, z, t, cropWidth, cropHeight, 1, 1);
					if (cropWidth <= 0 || cropHeight <= 0)
					{
						System.out.println("Warning: detection outside of image");
						this.gaussianAmplitude[c].add(t, 0);
						this.gaussianBackground[c].add(t, 0);
					}
					else
					{
					Sequence crop = SequenceUtil.getSubSequence(sequence, new Rectangle5D.Integer(minX, minY, z, t, c, cropWidth, cropHeight, 1, 1, 1));
					if (crop != null)
					{
						double[] cropImage = (double[]) ArrayUtil.arrayToDoubleArray(crop.getImage(0, 0, 0).getDataXY(0), crop.isSignedDataType());
						// build the series of exponential sum sum exp(-((x - minX)^2 + (y - minY)^2)/(2*standardDeviation^2))
						double sumExp = 0;
						double sumExp2 = 0;
						double sumExpVal = 0;
						double sumVal = 0;
						double varGaussian2 = 2 * stdGaussian * stdGaussian; 
						for (int y = 0; y < cropHeight; y++)
							for (int x = 0; x < cropWidth; x++)
							{
								double valExp  = Math.exp(-((y - pCropY)*(y - pCropY) + (x - pCropX)*(x - pCropX))/varGaussian2);
								sumExp += valExp;
								sumExp2 += (valExp * valExp);
								double val = cropImage[x + y*cropWidth];
								sumVal += val;
								sumExpVal += (val*valExp);
							}
						double detMat = sumExp2 * cropHeight * cropWidth - sumExp * sumExp;
						double a11 = cropHeight * cropWidth / detMat;
						double a12 = - sumExp / detMat;
						double a21 = a12;
						double a22 = sumExp2 / detMat;

						double amplitude = a11*sumExpVal + a12*sumVal;
						double background = a21*sumExpVal + a22*sumVal;

						this.gaussianAmplitude[c].add(t, amplitude);
						this.gaussianBackground[c].add(t, background);
					}
					else
					{
						this.gaussianAmplitude[c].add(t, 0);
						this.gaussianBackground[c].add(t, 0);
					}
				}
				}
			}

		}
	}
}
