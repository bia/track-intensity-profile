package plugins.nchenouard.trackprocessorintensityprofile;

import icy.sequence.Sequence;
import plugins.fab.trackmanager.TrackSegment;


/** Object to manage intensity values of a TrackSegment through time
 * 
 * @author nicolas chenouard
 * 
 * @date 11/03/2012
 *
 * */

abstract class TrackAnalysis
{
	TrackSegment track;
	Sequence sequence;
	String description;
	AveragingType averagingType;
	
	int halfCropSize = 5;

	TrackAnalysis(TrackSegment track, Sequence sequence, String description, AveragingType averagingMethod)
	{
		if (track == null)
			throw new IllegalArgumentException("NULL TrackSegment object is not allowed for creating a TrackAnalysis instance");
		if (sequence == null)
			throw new IllegalArgumentException("NULL Sequence object is not allowed for creating a TrackAnalysis instance");
		if (description == null)
			throw new IllegalArgumentException("NULL Sequence object is not allowed for creating a TrackAnalysis instance");
		this.track = track;
		this.sequence = sequence;
		this.description = description;
		this.averagingType = averagingMethod;
	};

	abstract public void clearSeries();
	
//	public void fillSeries(double diskRadius, double stdGaussian)
//	{
//		for (Detection d:track.getDetectionList())
//		{
//			ArrayList<Double> valueList = new ArrayList<Double>();
//			for (int c = 0; c < sequence.getSizeC(); c++)
//			{
//				valueList.clear();
//				int z = (int)Math.round(d.getZ());
//				int t = d.getT();
//				IcyBufferedImage image = sequence.getImage(t, z);
//				int cntValues = 0;
//				double sumIntensity = 0;
//				double minIntensity = 0;
//				double maxIntensity = 0;
//				double sumSqIntensity = 0;
//
//				int minX = (int) Math.max(0, d.getX() - diskRadius);
//				int minY = (int) Math.max(0, d.getY() - diskRadius);
//				int maxX = (int) Math.min(sequence.getSizeX() - 1, Math.ceil(d.getX() + diskRadius));
//				int maxY = (int) Math.min(sequence.getSizeY() - 1, Math.ceil(d.getY() + diskRadius));
//				for (int y = minY; y <= maxY; y++)
//					for (int x = minX; x <= maxX; x++)					
//					{
//						if ((d.getX() - x)*(d.getX() - x) + (d.getY() - y)*(d.getY() - y) <= diskRadius*diskRadius)
//						{
//							cntValues++;
//							double value  = image.getData(x, y, c);
//							sumIntensity += value;
//							if (cntValues == 1)
//							{
//								minIntensity = value;
//								maxIntensity = value;
//							}
//							else
//							{
//								if (value < minIntensity)
//									minIntensity = value;
//								if (value > maxIntensity)
//									maxIntensity = value;
//							}
//							sumSqIntensity += value*value;
//							valueList.add(new Double(value));
//						}
//					}
//				double meanIntensity = 0;
//				double varIntensity = 0;
//				if (cntValues >0 )
//				{
//					meanIntensity = sumIntensity/cntValues;
//					varIntensity = sumSqIntensity/cntValues - meanIntensity*meanIntensity;
//				}
//				this.meanIntensity[c].add(t, meanIntensity);
//				this.maxIntensity[c].add(t, maxIntensity);
//				this.minIntensity[c].add(t, minIntensity);
//				this.varIntensity[c].add(t, varIntensity);
//				this.sumIntensity[c].add(t, sumIntensity);
//				Arrays.sort(valueList.toArray());			
//				if (cntValues == 1)
//					this.medianIntensity[c].add(t, valueList.get(0));
//				else if (cntValues > 0)
//				{
//					if (cntValues%2 == 0)
//					{
//						this.medianIntensity[c].add(t, (valueList.get((int) cntValues/2))+(valueList.get(((int) cntValues/2))-1)/2);
//					}
//					else
//						this.medianIntensity[c].add(t, valueList.get((int) cntValues/2));
//				}
//				else
//					this.medianIntensity[c].add(t, 0);		
//				// compute the Gaussian amplitude				
//				minX = (int) d.getX() - halfCropSize;
//				double pCropX = halfCropSize + (d.getX() - (int) d.getX());
//				if (minX < 0)
//				{
//					pCropX -= minX;
//					minX = 0;
//				}
//				minY =  (int) d.getY() - halfCropSize;
//				double pCropY = halfCropSize + (d.getY() - (int) d.getY());
//				if (minY < 0)
//				{
//					pCropY -= minY;
//					minY = 0;
//				}
//				maxX = (int) Math.min(Math.ceil(d.getX()) + halfCropSize, sequence.getSizeX() - 1);
//				maxY = (int) Math.min(Math.ceil(d.getY()) + halfCropSize, sequence.getSizeY() - 1);
//				int cropWidth = maxX - minX + 1;
//				int cropHeight = maxY - minY + 1;
//
//				// fit the model background + amplitude*GaussianProfile to the cropped image around the detected position
//				Sequence crop = SequenceUtil.getSubSequence(sequence, minX, minY, z, t, cropWidth, cropHeight, 1, 1);
//				double[] cropImage = (double[]) ArrayUtil.arrayToDoubleArray(crop.getImage(0, 0, c).getDataXY(0), crop.isSignedDataType());
//				// build the series of exponential sum sum exp(-((x - minX)^2 + (y - minY)^2)/(2*standardDeviation^2))
//				double sumExp = 0;
//				double sumExp2 = 0;
//				double sumExpVal = 0;
//				double sumVal = 0;
//				double varGaussian2 = 2 * stdGaussian * stdGaussian; 
//				for (int y = 0; y < cropHeight; y++)
//					for (int x = 0; x < cropWidth; x++)
//					{
//						double valExp  = Math.exp(-((y - pCropY)*(y - pCropY) + (x - pCropX)*(x - pCropX))/varGaussian2);
//						sumExp += valExp;
//						sumExp2 += (valExp * valExp);
//						double val = cropImage[x + y*cropWidth];
//						sumVal += val;
//						sumExpVal += (val*valExp);
//					}
//				double detMat = sumExp2 * cropHeight * cropWidth - sumExp * sumExp;
//				double a11 = cropHeight * cropWidth / detMat;
//				double a12 = - sumExp / detMat;
//				double a21 = a12;
//				double a22 = sumExp2 / detMat;
//
//				double amplitude = a11*sumExpVal + a12*sumVal;
//				double background = a21*sumExpVal + a22*sumVal;
//
//				this.gaussianAmplitude[c].add(t, amplitude);
//				this.gaussianBackground[c].add(t, background);
//			}
//		}
//	}
}