package plugins.nchenouard.trackprocessorintensityprofile;

import java.util.ArrayList;
import java.util.Arrays;

import icy.sequence.Sequence;

import org.jfree.data.xy.XYSeries;

import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.particletracking.DetectionSpotTrack;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.Point3D;

public class TrackAnalysisMaskAveraging extends TrackAnalysis
{
	XYSeries[] meanIntensity;
	XYSeries[] minIntensity;
	XYSeries[] maxIntensity;
	XYSeries[] medianIntensity;
	XYSeries[] sumIntensity;
	XYSeries[] varIntensity;

	TrackAnalysisMaskAveraging(TrackSegment track, Sequence sequence,
			String description, AveragingType averagingMethod) {
		super(track, sequence, description, averagingMethod);

		meanIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			meanIntensity[c] = new XYSeries(description);
		minIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			minIntensity[c] = new XYSeries(description);
		maxIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			maxIntensity[c] = new XYSeries(description);
		medianIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			medianIntensity[c] = new XYSeries(description);
		sumIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			sumIntensity[c] = new XYSeries(description);
		varIntensity = new XYSeries[sequence.getSizeC()];
		for (int c = 0; c < sequence.getSizeC(); c++)
			varIntensity[c] = new XYSeries(description);
	}

	@Override
	public void clearSeries() {
		for (int c = 0; c < meanIntensity.length; c++)
			meanIntensity[c].clear();
		for (int c = 0; c < minIntensity.length; c++)
			minIntensity[c].clear();
		for (int c = 0; c < maxIntensity.length; c++)
			maxIntensity[c].clear();
		for (int c = 0; c < medianIntensity.length; c++)
			medianIntensity[c].clear();
		for (int c = 0; c < sumIntensity.length; c++)
			sumIntensity[c].clear();
		for (int c =  0; c < varIntensity.length; c++)
			varIntensity[c].clear();
	}

	public void fillAveragingSeriesAtT(int t, double[][][] imageTab, double diskRadius)
	{
		if (track.getFirstDetection().getT()<= t && track.getLastDetection().getT() >= t)
		{
			Detection d = track.getDetectionAtTime(t);
			if (d != null)
			{
				if (d instanceof DetectionSpotTrack && !((DetectionSpotTrack) d).spot.point3DList.isEmpty())
				{
					int width = sequence.getSizeX();
					int height = sequence.getSizeY();
					int depth = sequence.getSizeZ();
					DetectionSpotTrack dSpot = (DetectionSpotTrack) d;
					ArrayList<Double> valueList = new ArrayList<Double>();
					
					for (int c = 0; c < sequence.getSizeC(); c++)
					{
						valueList.clear();
						int cntValues = 0;
						double sumIntensity = 0;
						double minIntensity = 0;
						double maxIntensity = 0;
						double sumSqIntensity = 0;
						
						for (Point3D p:dSpot.spot.point3DList)
						{
							int x = (int)Math.round(p.x);
							int y = (int)Math.round(p.y);
							int z = (int)Math.round(p.z);

							if (x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < depth)
							{
								cntValues++;
								double value  = imageTab[z][c][x + y*width];
								sumIntensity += value;
								if (cntValues == 1)
								{
									minIntensity = value;
									maxIntensity = value;
								}
								else
								{
									if (value < minIntensity)
										minIntensity = value;
									if (value > maxIntensity)
										maxIntensity = value;
								}
								sumSqIntensity += value*value;
								valueList.add(new Double(value));
							}
						}
						double meanIntensity = 0;
						double varIntensity = 0;
						if (cntValues >0 )
						{
							meanIntensity = sumIntensity/cntValues;
							varIntensity = sumSqIntensity/cntValues - meanIntensity*meanIntensity;
						}
						this.meanIntensity[c].add(t, meanIntensity);
						this.maxIntensity[c].add(t, maxIntensity);
						this.minIntensity[c].add(t, minIntensity);
						this.varIntensity[c].add(t, varIntensity);
						this.sumIntensity[c].add(t, sumIntensity);
						Arrays.sort(valueList.toArray());			
						if (cntValues == 1)
							this.medianIntensity[c].add(t, valueList.get(0));
						else if (cntValues > 0)
						{
							if (cntValues%2 == 0)
							{
								this.medianIntensity[c].add(t, (valueList.get((int) cntValues/2))+(valueList.get(((int) cntValues/2))-1)/2);
							}
							else
								this.medianIntensity[c].add(t, valueList.get((int) cntValues/2));
						}
						else
							this.medianIntensity[c].add(t, 0);
					}
				}
				else
				{
					for (int c = 0; c < sequence.getSizeC(); c++)
					{
						int x = (int)Math.round(d.getX());
						int y = (int)Math.round(d.getY());
						int z = (int)Math.round(d.getZ());

						if (x >= 0 && x < sequence.getSizeX() && y >= 0 && y < sequence.getSizeY() && z >= 0 && z < sequence.getSizeZ())
						{
							double value  = imageTab[z][c][x + y*sequence.getSizeX()];
							this.meanIntensity[c].add(t, value);
							this.maxIntensity[c].add(t, value);
							this.minIntensity[c].add(t, value);
							this.varIntensity[c].add(t, 0);
							this.sumIntensity[c].add(t, value);
							this.medianIntensity[c].add(t, value);
						}
						else
						{
							this.meanIntensity[c].add(t, 0);
							this.maxIntensity[c].add(t, 0);
							this.minIntensity[c].add(t, 0);
							this.varIntensity[c].add(t, 0);
							this.sumIntensity[c].add(t, 0);
							this.medianIntensity[c].add(t, 0);
						}
					}
				}
			}
			else
			{
				for (int c = 0; c < sequence.getSizeC(); c++)
				{
					this.meanIntensity[c].add(t, 0);
					this.maxIntensity[c].add(t, 0);
					this.minIntensity[c].add(t, 0);
					this.varIntensity[c].add(t, 0);
					this.sumIntensity[c].add(t, 0);
					this.medianIntensity[c].add(t, 0);
				}
			}
		}
	}
}
